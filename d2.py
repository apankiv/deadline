import itertools
from copy import copy
import random

class Solve(object):
    def __init__(self, points, order):
        self.points = points
        self.order = order

    def l_i(self, line1, line2):
        p1, p2 = line1
        p3, p4 = line2
        x = False
        y = False

        if (max(p1[0], p2[0]) - min(p1[0], p2[0]) > max(p3[0], p4[0]) - min(p3[0], p4[0])):
            if max(p1[0], p2[0]) > max(p3[0], p4[0]) > min(p1[0], p2[0]) or max(p1[0], p2[0]) > min(p3[0], p4[0]) > min(p1[0], p2[0]):
                x = True
        else:
            if max(p3[0], p4[0]) > max(p1[0], p2[0]) > min(p3[0], p4[0]) or max(p4[0], p3[0]) > min(p1[0], p2[0]) > min(p3[0], p4[0]):
                x = True

        if (max(p1[1], p2[1]) - min(p1[1], p2[1]) > max(p3[1], p4[1]) - min(p3[1], p4[1])):
            if max(p1[1], p2[1]) > max(p3[1], p4[1]) > min(p1[1], p2[1]) or max(p1[1], p2[1]) > min(p3[1], p4[1]) > min(p1[1], p2[1]):
                y = True
        else:
            if max(p3[1], p4[1]) > max(p1[1], p2[1]) > min(p3[1], p4[1]) or max(p4[1], p3[1]) > min(p1[1], p2[1]) > min(p3[1], p4[1]):
                y = True

        if x and y:
            if p1 != p2 and p1 != p3 and p1 != p4 and p2 != p3 and p3 != p4 and p2 != p4:
                return True

        return False

    def run(self):
        res = 0
        pairs = []

        for x in range(len(pairs)-1):
            y = x+2
            while y < len(pairs):
                if x != y:
                    if self.l_i((self.points[pairs[x][0]], self.points[pairs[x][1]]), (self.points[pairs[y][0]], self.points[pairs[y][1]])):
                        return None
                y += 1

        for i in range(len(self.order)):
            pairs.append((self.order[len(self.order) - 1 - i], self.order[len(self.order) - 2 - i]))
            x1, y1 = self.points[self.order[len(self.order) - 1 - i]]
            x2, y2 = self.points[self.order[len(self.order) - 2 - i]]
            res += (y1 + y2) / 2 * (x1 - x2)
        res = abs(res)

        return res if res > 0 else None

T, = map(int, input().split())

for i in range(T):
    N, K = map(int, input().split())
    indicies = []
    points = {}

    for i in range(N):
        a, b, c = map(int, input().split())
        indicies.append(a)
        points[a] = (b, c)

    minimum = 10e11
    maximum = 0
    maxset = []
    minset = []

    for a in range(100000):
        b = Solve(points, indicies)
        random.shuffle(indicies)
        res = b.run()

        if not (res is None):
            if res >= maximum:
                maximum = copy(res)
                maxset = copy(indicies)
            if res <= minimum:
                minimum = copy(res)
                minset = copy(indicies)

    print(str(len(maxset)) + ' ' + ' '.join(map(str, maxset)))
    print(str(len(minset)) + ' ' + ' '.join(map(str, minset)))
    print(int(10 * (maximum - minimum)))
